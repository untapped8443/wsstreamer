#pragma once

#include <string>
#include <fstream>
#include <vector>

#include "boost/shared_ptr.hpp"
#include "boost/asio.hpp"
#include "boost/thread.hpp"
#include "websocketpp.hpp"
#include "websocket_connection_handler.hpp"

#include "utils.h"

using namespace std;

#ifdef _MSC_VER
#if _MSC_VER < 1600
using boost::shared_ptr;
#endif
#endif

// Server that streams data through WebSocket. Client specifies how many bytes it wants.
class WSStreamerHandler : public websocketpp::connection_handler {
private:
	map<string, vector<websocketpp::session_ptr>> connections;
	boost::mutex connections_mutex;
	ofstream debug;

public:
	WSStreamerHandler();
	~WSStreamerHandler();

	// Websocket++ callback functions
	void validate(websocketpp::session_ptr client); 
	void on_open(websocketpp::session_ptr client);
	void on_close(websocketpp::session_ptr client);
	void on_message(websocketpp::session_ptr client, const string& msg);
	void on_message(websocketpp::session_ptr client, const vector<unsigned char>& data);

	// Functions to add/remove accepted resources
	void addResource(string rsrc);
	void removeResource(string rsrc);

	// Sends to all clients who requested the specified resource
	void sendAll(string resource, const vector<unsigned char>& data);
	void sendAll(string resource, const unsigned char* data, int len);
};

class WSStreamerCtx {
public:
	boost::shared_ptr<WSStreamerHandler> streamer;
	boost::asio::io_service io_service;
	tcp::endpoint endpoint;
	websocketpp::server_ptr server;
	shared_ptr<boost::thread> iosrv_thread;

	WSStreamerCtx(string host, string port);
};